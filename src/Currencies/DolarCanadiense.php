<?php
namespace SatSuite\NumberToWords\Currencies;

use SatSuite\NumberToWords\Currencies\Currency;

class DolarCanadiense extends Currency
{
    public function getName()
    {
        return 'CAD';
    }

    public function getMeta()
    {
        return [
            'singular' => 'DÓLAR',
            'plural' => 'DÓLARES',
            'prefix' => 'CAD',
            'sufix' => 'CAD',
            'symbol' => '$',
        ];
    }
}